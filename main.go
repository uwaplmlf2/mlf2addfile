// Mlf2addfile adds one or more data files to the message queue.
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"golang.org/x/crypto/ssh/terminal"
)

const (
	DataExchange    = "data"
	CommandExchange = "commands"
)

const USAGE = `
mlf2addfile [options] N filename [filename ...]

Add one or more files to the message queue associated with float N.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showvers           = flag.Bool("version", false, "Show program version information and exit")
	server      string = "50.18.188.61:5672"
	credentials string
)

type FileMatch struct {
	Pattern  *regexp.Regexp
	Mimetype string
	Encoding string
	Newext   string
}

var FileMatcher []FileMatch
var QuickLookFile *regexp.Regexp

// Compile the file matching regular expressions
func init() {
	table := [][4]string{
		{`(?i).*\.ncz`, "application/x-netcdf", "gzip", ".nc"},
		{`(?i).*\.nc\.gz`, "application/x-netcdf", "gzip", ".nc"},
		{`(?i).*\.nc$`, "application/x-netcdf", "", ""},
		{`(?i)syslog\d*.txt$|errors\.txt$`, "text/x-syslog", "", ""},
		{`(?i)syslog\d*\.z|errors\.z|sbclog\.z`, "text/x-syslog", "gzip", ".txt"},
		{`(?i)syslog\d*\.txt\.gz|errors\.txt\.gz`, "text/x-syslog", "gzip", ""},
		{`(?i)bal\d*\.txt`, "text/csv", "", ""},
		{`(?i)sbclog_.*\.gz$`, "text/x-unixlog", "gzip", ".txt"},
		{`(?i)sbclog_.*\.txt$`, "text/x-unixlog", "", ""},
		{`(?i).*\.txt$`, "text/plain", "", ""},
		{`(?i).*\.txt.gz$`, "text/plain", "gzip", ""},
		{`(?i).*\.sx$`, "text/x-sexp", "", ""},
		{`(?i).*\.sxz`, "text/x-sexp", "gzip", ".sx"},
		{`(?i).*\.csv$`, "text/csv", "", ""},
		{`(?i).*\.xml`, "application/xml", "", ""},
		{`(?i)trk\d*\.z|eng\d*\.z|alt\d*\.z|bal\d*\.z`, "text/csv", "gzip", ".csv"},
		{`(?i)eng\d*\.csv\.gz`, "text/csv", "gzip", ".csv"},
		{`(?i).*\.jpg`, "image/jpeg", "", ""},
		{`(?i)00index$`, "text/x-listing", "", ""},
		{`(?i)00index\.z`, "text/x-listing", "gzip", ".txt"},
		{`(?i)00index\.gz`, "text/x-listing", "gzip", ".txt"},
		{`(?i).*\.tgz`, "application/x-tar", "", ""},
		{`(?i).*\.zip`, "application/zip", "", ""},
		{`(?i).*\.ntz`, "application/octet-stream", "gzip", ".ntk"},
		{`(?i).*\.jsz`, "application/json", "gzip", ".json"},
		{`(?i).*\.jsn\.gz`, "application/json", "gzip", ".json"},
		{`(?i).*\.jsn$`, "application/json", "", ".json"},
		{`(?i).*\.ndjson$`, "application/x-ndjson", "", ".ndjson"},
		{`(?i).*\.ndjson\.gz`, "application/x-ndjson", "gzip", ".ndjson"},
	}

	QuickLookFile = regexp.MustCompile(`(?i)telem\d*.*|dataql.*`)
	FileMatcher = make([]FileMatch, 0, len(table))
	for _, e := range table {
		FileMatcher = append(FileMatcher,
			FileMatch{
				Pattern:  regexp.MustCompile(e[0]),
				Mimetype: e[1],
				Encoding: e[2],
				Newext:   e[3],
			})
	}
}

type rawStatus struct {
	Mimetype string
	Contents []byte
}

// Guess the type and encoding of an MLF2 file
func guessType(filename string) (string, string, string) {
	for _, fm := range FileMatcher {
		if fm.Pattern.MatchString(filename) {
			return fm.Mimetype, fm.Encoding, fm.Newext
		}
	}

	return "application/octet-stream", "", ""
}

// Publish the float status message to an AMQP broker
func publishStatus(ch *amqp.Channel, floatid int, s *rawStatus, headers amqp.Table) error {
	key := fmt.Sprintf("float-%d.status", floatid)
	msg := amqp.Publishing{
		Headers:         headers,
		ContentType:     s.Mimetype,
		DeliveryMode:    2,
		Timestamp:       time.Now(),
		ContentEncoding: "us-ascii",
		Body:            s.Contents,
	}

	log.Printf("Publishing %s status message to %q", s.Mimetype, key)
	return ch.Publish(
		DataExchange,
		key,
		false, // mandatory
		false, // immediate
		msg)
}

// Publish a float file to an AMQP broker
func publishFile(ch *amqp.Channel, floatid int, name string, data []byte,
	headers amqp.Table) error {

	mtype, enc, ext := guessType(name)

	var filename string
	// Generate a unique filename for "quick look" data files
	if QuickLookFile.MatchString(name) {
		filename = fmt.Sprintf("ql-%s%s",
			time.Now().UTC().Format("20060102_150405"),
			filepath.Ext(name))
	} else {
		filename = name
	}

	if ext == "" {
		headers["filename"] = filename
	} else {
		if i := strings.LastIndex(filename, "."); i != -1 {
			headers["filename"] = filename[0:i] + ext
		}
	}
	defer delete(headers, "filename")

	key := fmt.Sprintf("float-%d.file", floatid)
	msg := amqp.Publishing{
		Headers:         headers,
		ContentType:     mtype,
		DeliveryMode:    2,
		Timestamp:       time.Now(),
		ContentEncoding: enc,
		Body:            data,
	}

	log.Printf("Publishing %q (%s) to %q", filename, mtype, key)
	return ch.Publish(
		DataExchange,
		key,
		false, // mandatory
		false, // immediate
		msg)
}

func mqSetup(uri string, queues map[string]string) (*amqp.Channel, error) {
	conn, err := amqp.Dial(uri)
	if err != nil {
		return nil, errors.Wrap(err, "connect")
	}

	channel, err := conn.Channel()
	if err != nil {
		return nil, errors.Wrap(err, "open channel")
	}

	for qname, exch := range queues {
		if err = channel.ExchangeDeclare(
			exch,    // name of the exchange
			"topic", // type
			true,    // durable
			false,   // delete when complete
			false,   // internal
			false,   // noWait
			nil,     // arguments
		); err != nil {
			return nil, errors.Wrap(err, "exchange declare")
		}

		state, err := channel.QueueDeclare(
			qname, // name of the queue
			true,  // durable
			false, // delete when usused
			false, // exclusive
			false, // noWait
			nil,   // arguments
		)
		if err != nil {
			return nil, errors.Wrap(err, "queue declare")
		}

		if err = channel.QueueBind(
			state.Name, // name of the queue
			state.Name, // bindingKey
			exch,       // sourceExchange
			false,      // noWait
			nil,        // arguments
		); err != nil {
			return nil, errors.Wrap(err, "queue bind")
		}
	}

	return channel, nil
}

func getPassword(prompt string) ([]byte, error) {
	fd := int(os.Stdin.Fd())
	state, err := terminal.GetState(fd)
	if err != nil {
		return nil, err
	}

	ch := make(chan os.Signal)
	signal.Notify(ch, os.Interrupt, os.Kill)
	defer signal.Stop(ch)

	go func() {
		_, ok := <-ch
		if ok {
			terminal.Restore(fd, state)
			os.Exit(1)
		}
	}()

	fmt.Print(prompt)
	pass, err := terminal.ReadPassword(fd)
	fmt.Println("")

	return pass, err
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, USAGE)
		flag.PrintDefaults()
	}

	flag.StringVar(&server, "server",
		lookupEnvOrString("ADDFILE_SERVER", server),
		"host:port for message broker server")
	flag.StringVar(&credentials, "user",
		lookupEnvOrString("ADDFILE_CREDS", credentials),
		"message broker username:password")

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	floatid, err := strconv.Atoi(args[0])
	if err != nil {
		log.Fatalf("Cannot parse float-ID: %v", err)
	}

	u := url.URL{}
	u.Scheme = "amqp"
	u.Host = server
	u.Path = "mlf2"

	if credentials != "" {
		creds := strings.Split(credentials, ":")
		switch len(creds) {
		case 1:
			pass, err := getPassword("Message-queue password: ")
			if err != nil {
				log.Fatal(err)
			}
			u.User = url.UserPassword(creds[0], string(pass))
		case 2:
			u.User = url.UserPassword(creds[0], creds[1])
		}
	}

	queues := make(map[string]string)
	queues["float.status"] = DataExchange
	queues["float.file"] = DataExchange

	channel, err := mqSetup(u.String(), queues)
	if err != nil {
		log.Fatal(err)
	}

	headers := make(amqp.Table)
	headers["floatid"] = int32(floatid)

	for _, pathname := range args[1:] {
		name := path.Base(pathname)
		_, enc, _ := guessType(name)
		if enc == "gzip" {
			log.Printf("Skipping compressed file: %q", pathname)
			continue
		}
		contents, err := ioutil.ReadFile(pathname)
		if err != nil {
			log.Printf("Error reading %q: %v", pathname, err)
			continue
		}

		if name == "status.xml" {
			err = publishStatus(channel, int(floatid),
				&rawStatus{Mimetype: "application/xml", Contents: contents},
				headers)
		} else {
			err = publishFile(channel, int(floatid), name, contents, headers)
		}
		if err != nil {
			log.Printf("File publishing error: %v", err)
		}
	}
}
