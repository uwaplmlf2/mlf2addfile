# MLF2 File Uploader

This application uploads one or more data files to the MLF2 [CouchDB](http://couchdb.apache.org) database. The files are uploaded by "publishing" them to an [AMQP Message Broker](https://en.wikipedia.org/wiki/Advanced_Message_Queuing_Protocol), this allows for pre-processing of the files (e.g. converting the S-exp files to netCDF) before they are stored.

## Installation

Download a binary release for Linux, MacOS, or Windows from the [Downloads section](https://bitbucket.org/uwaplmlf2/mlf2addfile/downloads/) of this repository and extract onto your system. All binaries are distributed in gzip-compressed TAR archives.

## Usage

``` shellsession
$ mlf2addfile --help

mlf2addfile [options] N filename [filename ...]

Add one or more files to the message queue associated with float N.
  -server string
    	host:port for message broker server (default "50.18.188.61:5672")
  -user string
    	message broker username:password
  -version
    	Show program version information and exit
```
