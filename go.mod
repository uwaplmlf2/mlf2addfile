module bitbucket.org/uwaplmlf2/mlf2addfile

require (
	github.com/pkg/errors v0.8.1
	github.com/streadway/amqp v0.0.0-20190404075320-75d898a42a94
	golang.org/x/crypto v0.0.0-20191119213627-4f8c1d86b1ba
	golang.org/x/sys v0.0.0-20220704084225-05e143d24a9e // indirect
)

go 1.13
